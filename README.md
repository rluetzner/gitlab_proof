# Gitlab Proof

[Verifying my OpenPGP key: openpgp4fpr:CFD0C4A5288544168EEF3982DAE5F7985B7DC1E0]

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://docs.keyoxide.org/advanced/openpgp-proofs/
